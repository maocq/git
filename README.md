# GIT

```sh
git --version
git help
git config --global user.name "maocq"
git config --global user.email "a@gmail.com"
git config --global --list
```

Crear un repositorio Git vacío o reiniciar una ya existente
```sh
git init
```

Clone a repository into a new directory
```sh
git clone <>
```

Muestran el estado del árbol de trabajo
```sh
git status
```

Diferencias
```sh
git diff -- myfile.txt
```

Añadir añadir el contenido del archivo al índice
```sh
git add *
```

Se registran los cambios en el repositorio
```sh
git commit -m "Mensaje"
```

add + commit
```sh
git commit -am "Mensaje"
```

Mostrar registros
```sh
git log
git log > commits.txt
```

```sh
git log --oneline --graph --all
```

Cambiar ramas o restaurar los archivos del árbol de trabajo
```sh
git checkout master
git checkout <commit>/<branch>
```

```sh
git reset --soft <commit>
git reset --mixed <commit>
git reset --hard <commit>
```

### Brach
```sh
git branch
```

Nuevo branch
```sh
git branch <name>
```

Merge 
```sh
git merge <branch>
git merge --no-ff <branch>
```

Eliminar branch
```sh
git branch -d <name>
```


### Remote
```sh
git remote add origin <>
```

```sh
git push origin master
```

```sh
git fecth origin
```

```sh
git merge origin/master
```

> [GitFlow](GITFLOW.md)