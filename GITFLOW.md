# GitFlow

Inicio de git-flow
```sh
git flow init
```

### Creación de una nueva funcionalidad o corrección de un error
```sh
git flow feature start <feature>
```

Publicando la nueva rama
```sh
git flow feature publish <feature>
```

Finalización del feature
```sh
git flow feature finish <feature>
```


### Release AAAA-MM-DD.# (merge a master)
```sh
git flow release start <release>
```

Una vez terminado el QA,
```sh
git flow release finish <release> 
```



### Corrección de errores
```sh
git flow hotfix start <release> 
```

```sh
git flow hotfix finish <release>
```